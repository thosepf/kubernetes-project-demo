package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	httpSwagger "github.com/swaggo/http-swagger"

	redis "github.com/go-redis/redis/v7"
)

// @title Polar Squad Colors Backend API
// @version 1.0
// @description This is a service that hosts a list of colors.
// @termsOfService https://polarsquad.com/

// @contact.name Contact
// @contact.url https://polarsquad.com/contact

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8080
// @BasePath /
func main() {
	logFormat := log.Flags() &^ (log.Ldate | log.Ltime)
	log.SetFlags(logFormat)
	middleware.DefaultLogger = middleware.RequestLogger(
		&middleware.DefaultLogFormatter{Logger: log.New(os.Stdout, "", logFormat), NoColor: true})

	err := initDatabase()
	if err != nil {
		log.Panicf("Could not initialize database (%s): %s", db.Options().Addr, err)
	}

	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("I like colors."))
	})

	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		error := HTTPError{Code: http.StatusNotFound, Message: "Not found"}
		output, _ := json.Marshal(error)

		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(output))
	})

	r.Get("/color", GetColor)
	r.Get("/color/{day:^([1-9]|[12][0-9]|3[01])$}", GetColor)

	r.Get("/colors", ListColors)
	r.Post("/colors/{name}/{hex:[0-9a-fA-F]{6}}", AddColor)
	r.Delete("/colors/{name}", DelColor)

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	log.Println("Server started on port 8080")
	http.ListenAndServe(":8080", r)
}

// HTTPError godoc
// @Summary Struct for representing a returned error as JSON
// @Description http error
type HTTPError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// Color godoc
// @Summary Struct for representing a color's information
// @Description color
type Color struct {
	Name string `json:"name"`
	Hex  string `json:"hex"`
}

// MarshalBinary -
func (c *Color) MarshalBinary() ([]byte, error) {
	return json.Marshal(c)
}

// UnmarshalBinary -
func (c *Color) UnmarshalBinary(b []byte) error {
	return json.Unmarshal(b, c)
}

var defaultColors = []*Color{
	{Name: "red", Hex: "#ff0000"},
	{Name: "green", Hex: "#008000"},
	{Name: "blue", Hex: "#0000ff"},
}

var db *redis.Client

func readConfig(key, _default string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return _default
}

func initDatabase() error {
	db = redis.NewClient(&redis.Options{
		Addr:     readConfig("COLORS_DATABASE_ADDRESS", "localhost:6379"),
		Password: "",
		DB:       0,
	})

	_, err := db.Ping().Result()
	if err != nil {
		return err
	}

	for _, c := range defaultColors {
		err = db.RPush("colors", c).Err()
		if err != nil {
			return err
		} else {
			log.Printf("Added %s to database", c.Name)
		}
	}

	return nil
}

// GetColor godoc
// @Summary Returns the color of the day
// @Description get color
// @Produce json
// @Param day path int false "Day"
// @Success 200 {object} Color
// @Router /color/{day} [get]
func GetColor(w http.ResponseWriter, r *http.Request) {
	day, err := strconv.Atoi(chi.URLParam(r, "day"))
	if err != nil {
		log.Println("No day given, that must mean you want today's color!")
		day = time.Now().Day()
	}

	var colors = []*Color{}

	err = db.LRange("colors", 0, -1).ScanSlice(&colors)
	if err != nil {
		log.Printf("Failed to retrieve colors from database %s", err)
	}

	color := colors[day%len(colors)]
	log.Printf("The color of the day for: %d is %s!\n", day, color.Name)

	output, _ := json.Marshal(color)
	w.Write([]byte(output))
}

// ListColors godoc
// @Summary Retuns all the available colors
// @Description get colors
// @Produce json
// @Success 200 {array} Color
// @Router /colors [get]
func ListColors(w http.ResponseWriter, r *http.Request) {
	var colors = []*Color{}

	err := db.LRange("colors", 0, -1).ScanSlice(&colors)
	if err != nil {
		log.Printf("Failed to retrieve colors from database %s", err)
	}

	output, _ := json.Marshal(colors)
	w.Write([]byte(output))
}

// AddColor godoc
// @Summary Adds a color to the list
// @Description add color
// @Produce json
// @Param name path string true "Name"
// @Param hex path string true "Hex"
// @Failure	400 {object} HTTPError
// @Success 200 {object} Color
// @Router /colors/{name}/{hex} [post]
func AddColor(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	hex := fmt.Sprintf("#%s", chi.URLParam(r, "hex"))

	var colors = []*Color{}

	err := db.LRange("colors", 0, -1).ScanSlice(&colors)
	if err != nil {
		log.Printf("Failed to retrieve colors from database %s", err)
	}

	for _, c := range colors {
		if name == c.Name {
			log.Printf("Color %s exists in database already.", c.Name)
			error := HTTPError{Code: http.StatusBadRequest, Message: "That color exists already."}
			output, _ := json.Marshal(error)

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(output))
			return
		}
	}

	color := Color{Name: name, Hex: hex}
	err = db.RPush("colors", &color).Err()
	if err != nil {
		log.Printf("Could not add color to database %s", err)
		error := HTTPError{Code: http.StatusInternalServerError, Message: "Could not add color to database."}
		output, _ := json.Marshal(error)

		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(output))
		return
	} else {
		log.Printf("Added %s to database", color.Name)
	}

	output, _ := json.Marshal(color)
	w.Write([]byte(output))
}

// DelColor godoc
// @Summary Deletes a color the list
// @Description delete color
// @Produce json
// @Param name path string true "Name"
// @Failure	400 {object} HTTPError
// @Success 200 {object} Color
// @Router /colors/{name} [delete]
func DelColor(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")

	var colors = []*Color{}

	err := db.LRange("colors", 0, -1).ScanSlice(&colors)
	if err != nil {
		log.Printf("Failed to retrieve colors from database %s", err)
	}

	for _, c := range colors {
		if name == c.Name {
			var output []byte
			err := db.LRem("colors", 0, c).Err()
			if err != nil {
				log.Printf("Could not delete color %s from database %s", c.Name, err)

				error := HTTPError{Code: http.StatusInternalServerError, Message: "Could not delete color from database."}
				output, _ = json.Marshal(error)
			} else {
				log.Printf("Deleted %s from database", c.Name)
				output, _ = json.Marshal(c)
			}

			w.Write([]byte(output))
			return
		}
	}

	log.Printf("Color %s did not exist in database", name)
	error := HTTPError{Code: http.StatusBadRequest, Message: "That color did not exist."}
	output, _ := json.Marshal(error)

	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(output))
}
